package storage

import (
	"encoding/json"
	"io/ioutil"

	"gitlab.com/uni-uni/siim-cctv-streaming/core/config"
)

func ReadConfig() *config.Config {
	file, _ := ioutil.ReadFile("config.json")
	data := &config.Config{}

	_ = json.Unmarshal([]byte(file), &data)

	return data
}

func StoreConfig(conf *config.Config) {
	file, _ := json.MarshalIndent(conf, "", " ")
	_ = ioutil.WriteFile("config.json", file, 0644)
}
