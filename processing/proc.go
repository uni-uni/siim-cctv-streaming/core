package processing

import (
	"bufio"
	"fmt"
	"gitlab.com/uni-uni/siim-cctv-streaming/core/config"
	"gitlab.com/uni-uni/siim-cctv-streaming/core/parser"
	"io"
	"os/exec"
	"time"
)

type Proc struct {
	conf *config.Config
	pc   *parser.ParsedConf
	cmd  *exec.Cmd
}

func NewProc(conf *config.Config) *Proc {
	pc := parser.NewParsedConf(conf)
	p := Proc{conf, pc, nil}
	return &p
}

func (p *Proc) StartStream() {
	p.pc.ParseConfig(p.conf)
	p.start()
}

func (p *Proc) start() {
	//ffmpeg -analyzeduration 20M -probesize 10M -i rtmp://127.0.0.1:1937/live/test -filter_complex [0][0]xstack=inputs=2:layout=0_0|0_h0[s0] -map [s0] -c:a aac -c:v libx264 -f flv -preset faster rtmp://localhost:1935/live/test -y
	p.cmd = exec.Command("ffmpeg",
		p.pc.Command()...)
	// TODO err
	stdout, _ := p.cmd.StdoutPipe()
	stderr, _ := p.cmd.StderrPipe()
	_ = p.cmd.Start()

	scanner := bufio.NewScanner(io.MultiReader(stdout, stderr))
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		m := scanner.Text()
		fmt.Println(m)
	}
	_ = p.cmd.Wait()
}

func (p *Proc) ReloadStream(conf *config.Config) {
	p.conf = conf
	p.pc.ParseConfig(conf)
	p.cmd.Process.Kill()
	time.Sleep(5)
	p.StartStream()
}

func (p *Proc) handlerErr(err error) {
	// TODO handle
	println("Stream error, how naive You are! No handling yet :(")
}
