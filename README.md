# core

It's the core camera streams `PROCESSING` API for CCTV, however can be used
with any RTMP capable video source.

The Idea here is to multiplex video streams from multiple sources into one stream
and allow the single stream output to be consumed externally.
We provide client web app (React) for this purpose [client-app](https://gitlab.com/uni-uni/siim-cctv-streaming/client-app), 
although any HLS/RTMP capable client can be used for consuming the stream, e.g. VLC.

## The idea

1. `STREAM SOURCE` sends RTMP stream to `NGINX` RTMP proxy.
2. There can be multiple `STREAM SOURCE`, each has dedicated `in` and `out` port on `NGINX`
3. `PROCESSING` reads from dedicated endpoints from `NGINX` RTMP proxy for each source it has configured.
4. `PROCESSING` multiplexes many source streams into one output stream using `ffmpeg`.
5. `PROCESSING` sends output stream to `NGINX`, which proxies the stream for the client/s.
6. `CLIENT/SINK` consumes the stream from `NGINX` output proxy.

NOTE, that in the final solution `REST API` and `PROCESSING` are implemented in this core project in separate package,
without inter-service HTTP communication.

![block_diagram(2).png](arch-overview.png)

## How to build and run

We tested on below versions of environment, however newer should be feasible:
- go1.22.4
- ffmpeg 7.0.2

**Build**

```sh
git clone https://gitlab.com/uni-uni/siim-cctv-streaming/core.git
cd core
go build .
```

**Run**
1. Run RTMP proxy for every source stream. We run separate container for each. The config can be found here [middleware](https://gitlab.com/uni-uni/siim-cctv-streaming/middleware).
2. Stream the video to RTMP source proxy. You can use OBS for testing.
3. Set stream sources in `config.json`. The source here is the output of nginx.
4. Run RTMP proxy for core `PROCESSING` output. Configuration from `1.` can be used again. The in endpoint 
for output proxy is hardoced for now `rtmp://localhost:1935/live-out/test` 
with no plan to change it in the near future, so the endpoint has to be exactly this.
5. Provide `config.json`. See example in this repo.
6. Run the `./core` - the `REST API` will also start serving requests on `localhost:8090` by default.
6. You can optionally modify the stream config using our [client app](https://gitlab.com/uni-uni/siim-cctv-streaming/client-app).
5. Access the stream using our client or any other HLS/RTMP capable streaming client, e.g. VLC.